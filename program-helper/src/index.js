// @ts-check

import path from 'path';
import { Gitlab } from '@gitbeaker/node';
import _ from 'lodash';
import fsp from 'fs/promises';
import fs from 'fs';
import { execSync } from 'child_process';
import os from 'os';
import git from 'isomorphic-git';

const getFullImageName = ({ projectName, projectNamespace, containerRegistry }) => {
  let programNamespaceElements = projectNamespace.split('/');
  let imageName = 'hexlet-program-source-ci';

  if (projectName !== 'hexlet-program-ci') {
    programNamespaceElements = programNamespaceElements.slice(0, -2);
    imageName = `${programNamespaceElements.at(-1)}-program`;
  }

  return [
    containerRegistry,
    ...programNamespaceElements,
    imageName,
  ].join('/');
};

const getLastSuccessCommit = async ({ token, projectId, branch }) => {
  const api = new Gitlab({
    token,
  });

  const [lastSuccessPipeline] = await api.Pipelines.all(
    projectId,
    { ref: branch, status: 'success' },
  );

  return _.get(lastSuccessPipeline, 'sha');
};

const getModifiedExercises = (sha1, sha2, projectDir) => {
  const diffCmd = `git diff --name-only ${sha1} ${sha2}`;
  const output = execSync(diffCmd, { cwd: projectDir }).toString().trim();

  if (_.isEmpty(output)) {
    return [];
  }

  const filePaths = output.split(os.EOL);
  const exercises = filePaths
    .filter((filePath) => /^exercises\/.*\//.test(filePath))
    .map((filePath) => filePath.split(path.sep)[1]);

  return _.uniq(exercises);
};

const getAllExercises = async (projectDir) => {
  const exercisesPath = path.join(projectDir, 'exercises');

  if (!fs.existsSync(exercisesPath)) {
    return [];
  }

  const items = await fsp.readdir(exercisesPath, { withFileTypes: true });
  return items
    .filter((item) => item.isDirectory())
    .map((exercise) => exercise.name);
};

const getTestableExercises = async (options) => {
  const {
    currentCommit,
    projectDir,
  } = options;

  const lastSuccessCommit = await getLastSuccessCommit(options);
  const allExercises = await getAllExercises(projectDir);

  const commitData = await git.log({ fs, dir: projectDir });
  const commits = commitData.map(({ oid }) => oid);

  const modifiedExercises = commits.includes(lastSuccessCommit)
    ? getModifiedExercises(lastSuccessCommit, currentCommit, projectDir)
    : allExercises;

  const testableExercises = _.intersection(modifiedExercises, allExercises);

  return testableExercises.join(os.EOL);
};

export { getFullImageName, getTestableExercises };
