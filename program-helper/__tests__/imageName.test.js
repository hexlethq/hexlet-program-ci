import { getFullImageName } from '../src/index.js';

it('self repository', () => {
  const options = {
    projectName: 'hexlet-program-ci',
    projectNamespace: 'hexlethq',
    containerRegistry: 'registry.example.org',
  };

  const fullImageName = getFullImageName(options);
  const expected = [
    'registry.example.org',
    'hexlethq',
    'hexlet-program-source-ci',
  ].join('/');
  expect(fullImageName).toEqual(expected);
});

it('repository with standard nesting of namespaces', () => {
  const options = {
    projectName: '1775365',
    projectNamespace: 'hexlethq/programs/java/hexlet-groups/java-17',
    containerRegistry: 'registry.example.org',
  };

  const fullImageName = getFullImageName(options);
  const expected = [
    'registry.example.org',
    'hexlethq/programs/java',
    'java-program',
  ].join('/');
  expect(fullImageName).toEqual(expected);
});
