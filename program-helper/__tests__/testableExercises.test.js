import os from 'os';
import path from 'path';
import { fileURLToPath } from 'url';
import nock from 'nock';

import { getTestableExercises } from '../src/index.js';

// git log
// 9b2c44e | 2021-05-12 | 79 seconds ago | remove file from funcdamentals (HEAD -> main) [Student]
// 3b278d7 | 2021-05-12 | 19 minutes ago | remove logic exercise [Student]
// c5d1d97 | 2021-05-12 | 60 seconds ago | add logic exercise [Student]
// da85018 | 2021-05-12 | 2 hours ago | add wrong file [Student]
// 0025342 | 2021-05-12 | 74 minutes ago | add methods-define exercise [Student]
// 6e2f25c | 2021-05-12 | 75 minutes ago | add funcdamentals exercise [Student]
// d71fefe | 2021-05-12 | 76 minutes ago | add start exercise [Student]
// a0472f3 | 2021-05-12 | 81 minutes ago | init [Student]

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const token = 'some-token';
const projectId = '125';
const branch = 'main';
const projectDir = path.join(__dirname, '..', '__fixtures__', 'repo-user');

const baseOptions = {
  token, projectId, branch, projectDir,
};

const mockRequest = (data) => {
  nock('https://gitlab.com/api/v4')
    .get(`/projects/${projectId}/pipelines?ref=${branch}&status=success`)
    .reply(200, data);
};

nock.disableNetConnect();

beforeEach(() => {
  nock.cleanAll();
});

it('init commit (no pipelines)', async () => {
  const options = {
    ...baseOptions,
    currentCommit: 'a0472f3b3436b088eed885d1050923d565730c89',
  };

  mockRequest([]);

  const testableExercises = await getTestableExercises(options);
  const expected = ['fundamentals', 'methods-define', 'start'].join(os.EOL);

  expect(testableExercises).toEqual(expected);
});

it('commit with start exercise', async () => {
  const options = {
    ...baseOptions,
    currentCommit: 'd71fefe4048b77f06efdf35c39f7729da5597110',
  };

  mockRequest([{ sha: 'a0472f3b3436b088eed885d1050923d565730c89' }]);

  const testableExercises = await getTestableExercises(options);
  const expected = 'start';

  expect(testableExercises).toEqual(expected);
});

it('two commits with two exercises', async () => {
  const options = {
    ...baseOptions,
    currentCommit: '0025342b90e317f6972aa6a7edecd50f3d317785',
  };

  mockRequest([{ sha: 'd71fefe4048b77f06efdf35c39f7729da5597110' }]);

  const testableExercises = await getTestableExercises(options);
  const expected = ['fundamentals', 'methods-define'].join(os.EOL);

  expect(testableExercises).toEqual(expected);
});

it('commit with some file into exercises dir (no changes in exercises)', async () => {
  const options = {
    ...baseOptions,
    currentCommit: 'da85018ead21b49f7721975be07dd44abf3ac3a2',
  };

  mockRequest([{ sha: '0025342b90e317f6972aa6a7edecd50f3d317785' }]);

  const testableExercises = await getTestableExercises(options);
  const expected = '';

  expect(testableExercises).toEqual(expected);
});

it('commit with add & remove exercise (no changes in exercises)', async () => {
  const options = {
    ...baseOptions,
    currentCommit: '3b278d7b20ea02689ea7d2a3a517c3e85cba40bd',
  };

  mockRequest([{ sha: 'da85018ead21b49f7721975be07dd44abf3ac3a2' }]);

  const testableExercises = await getTestableExercises(options);
  const expected = '';

  expect(testableExercises).toEqual(expected);
});

it('commit with only completly remove exercise (no changes in exercises)', async () => {
  const options = {
    ...baseOptions,
    currentCommit: '3b278d7b20ea02689ea7d2a3a517c3e85cba40bd',
  };

  mockRequest([{ sha: 'c5d1d97ec4441031626364a9f2af30750cef43f7' }]);

  const testableExercises = await getTestableExercises(options);
  const expected = '';

  expect(testableExercises).toEqual(expected);
});

it('commit with partial remove files from exercise', async () => {
  const options = {
    ...baseOptions,
    currentCommit: '9b2c44e061a48571fba212c4beee21f7345328ec',
  };

  mockRequest([{ sha: '3b278d7b20ea02689ea7d2a3a517c3e85cba40bd' }]);

  const testableExercises = await getTestableExercises(options);
  const expected = 'fundamentals';

  expect(testableExercises).toEqual(expected);
});

it('commit does not exist, but pipeline exists (force push)', async () => {
  const options = {
    ...baseOptions,
    currentCommit: '9b2c44e061a48571fba212c4beee21f7345328ec',
  };

  mockRequest([{ sha: 'non-existent-hash-commit' }]);

  const testableExercises = await getTestableExercises(options);
  const expected = ['fundamentals', 'methods-define', 'start'].join(os.EOL);

  expect(testableExercises).toEqual(expected);
});
