#!/usr/bin/env node

import { getFullImageName } from '../src/index.js';

const projectName = process.env.CI_PROJECT_NAME;
const projectNamespace = process.env.CI_PROJECT_NAMESPACE;
const containerRegistry = process.env.CI_REGISTRY;

const options = {
  projectName,
  projectNamespace,
  containerRegistry,
};

console.log(getFullImageName(options));
