#!/usr/bin/env node

import { getTestableExercises } from '../src/index.js';

const token = process.env.READ_API_TOKEN;
const projectId = process.env.CI_PROJECT_ID;
const branch = process.env.CI_COMMIT_REF_NAME;
const currentCommit = process.env.CI_COMMIT_SHA;
const projectDir = process.env.CI_PROJECT_DIR;

const options = {
  token,
  projectId,
  branch,
  currentCommit,
  projectDir,
};

getTestableExercises(options).then(console.log);
