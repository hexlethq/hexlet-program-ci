compose-setup: compose-build compose-install

compose-build:
	docker-compose build

compose-install:
	docker-compose run app make setup

compose-bash:
	docker-compose run app bash

compose-test:
	docker-compose run app make test

compose-lint:
	docker-compose run app make lint

setup:
	make setup -C program-helper

lint:
	make lint -C program-helper

test:
	make test -C program-helper

release:
	git push -f origin main:release
