FROM docker/compose:debian-1.29.2

RUN apt-get update && apt-get install -yq make curl git

RUN curl -fsSL https://deb.nodesource.com/setup_lts.x | bash -
RUN apt-get install -y nodejs

WORKDIR /usr/src/app

COPY . .

# NOTE: After local development using docker compos,
# the git directory remains.
RUN rm -rf program-helper/__fixtures__/repo-user/.git
RUN ln -s ../git-user program-helper/__fixtures__/repo-user/.git

RUN cd program-helper && npm ci
RUN cd program-helper && npm link
